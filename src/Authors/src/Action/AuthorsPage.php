<?php

namespace Authors\Action;

use Psr\Http\Message\ServerRequestInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;
use Authors\Model\AuthorRestCollection;
use App\Action\AbstractPage;
use Authors\Model\Author;
use Authors\Form\AuthorForm;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Description of AuthorsPage
 *
 * @author gabriel
 */
class AuthorsPage extends AbstractPage
{
    private $renderer;
    private $authorrestcollection;
    
    public function __construct(
            TemplateRendererInterface $renderer,
            AuthorRestCollection $authorrestcollection
            )
    {
        $this->renderer=$renderer;
        $this->authorrestcollection=$authorrestcollection;
    }
    
    public function indexAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $authors=$this->authorrestcollection->fetchAll();
        return new HtmlResponse(
            $this->renderer->render('author/view::authors-index',['authors'=>$authors])
        );
    }
    
    public function addAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $form=new AuthorForm();
        $form->get('submit')->setValue('Add');
        //$request=$this->getRequest();
        
        if($request->getMethod()!=='POST')
        {
            return new HtmlResponse(
                $this->renderer->render('author/view::authors-add',['form'=>$form])
            );
        }
        
        $author=new Author();
        $form->setInputFilter($author->getInputFilter());
        //throw new \Exception(print_r($request->getParsedBody(),1));
        $form->setData($request->getParsedBody());
        
        if(!$form->isValid())
        {
            return new HtmlResponse(
                $this->renderer->render('author/view::authors-add')
            );
        }
        
        $author->exchangeArray($form->getData());
        
        $this->authorrestcollection->saveAuthor($author);
        return new RedirectResponse("/author");
    }
    
    public function editAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $uri=$request->getUri()->getPath();
        $route=array();
        preg_match("/(.+)\/(.+)\/(.+)/", $uri, $route);
        $entity_id=(int) $route[3];
        if($entity_id===0)
        {
            return $this->redirect()->toRoute('author',['action'=>'add']);
        }
        
        try {
            $author=$this->authorrestcollection->getAuthor($entity_id);
        } catch (\Exception $ex) {
            return new RedirectResponse("/author");
        }
        
        $form=new AuthorForm();
        $form->bind($author);
        $form->get('submit')->setAttribute('value', 'Edit');
        
        $viewData=['entity_id'=>$entity_id,'author'=>$author];
        
        if($request->getMethod()!=='POST')
        {
            return new HtmlResponse(
                $this->renderer->render('author/view::authors-edit', $viewData)
            );
        }
        
        $form->setInputFilter($author->getInputFilter());
        $form->setData($request->getParsedBody());
        
        if(!$form->isValid())
        {
            return new HtmlResponse(
                $this->renderer->render('author/view::authors-edit', $viewData)
            );
        }
        
        $this->authorrestcollection->saveAuthor($author);
        
        return new RedirectResponse("/author");
    }
    
    public function deleteAction(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $uri=$request->getUri()->getPath();
        $route=array();
        preg_match("/(.+)\/(.+)\/(.+)/", $uri, $route);
        $entity_id=(int) $route[3];
        if($entity_id===0)
        {
            return $this->redirect()->toRoute('author',['action'=>'add']);
        }
        
        if($request->getMethod()==='POST')
        {
            $post=$request->getParsedBody();
            $del=$post['del'];
            if($del==='Yes')
            {
                $id=(int) $post['entity_id'];
                $this->authorrestcollection->deleteAuthor($id);
            }
            return new RedirectResponse("/author");
        }
        $author=$this->authorrestcollection->getAuthor($entity_id);
        return new HtmlResponse(
            $this->renderer->render("author/view::authors-delete", ['entity_id'=>$entity_id, 'author'=>$author])
        );
    }
}
