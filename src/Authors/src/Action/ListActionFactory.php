<?php

namespace Authors\Action;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Authors\Action\ListAction;

class ListActionFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return ListAction
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ListAction(
                $container->get(\Zend\Expressive\Template\TemplateRendererInterface::class),
                $container->get(\Authors\Model\AuthorRestCollection::class)
                );
    }
}
