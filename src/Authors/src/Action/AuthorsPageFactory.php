<?php

namespace Authors\Action;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Authors\Action\AuthorsPage;

/**
 * Description of AuthorsPageFactory
 *
 * @author gabriel
 */
class AuthorsPageFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return ListAction
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthorsPage(
                $container->get(\Zend\Expressive\Template\TemplateRendererInterface::class),
                $container->get(\Authors\Model\AuthorRestCollection::class)
                );
    }
}
