<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Authors\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;
use Authors\Model\AuthorRestCollection;

/**
 * Description of ListAction
 *
 * @author gabriel
 */
class ListAction implements MiddlewareInterface
{
    private $renderer;
    private $authorrestcollection;
    
    public function __construct(
            TemplateRendererInterface $renderer,
            AuthorRestCollection $authorrestcollection
            )
    {
        $this->renderer=$renderer;
        $this->authorrestcollection=$authorrestcollection;
    }
    
    /**
     * 
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return HtmlResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $authors=$this->authorrestcollection->fetchAll();
        return new HtmlResponse(
            $this->renderer->render('author/view::authors-list',['authors'=>$authors])
        );
    }
}
