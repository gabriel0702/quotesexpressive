<?php

namespace Authors;

/**
 * The configuration provider for the Authors module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
//                Authors\Action\ListAction::class => Authors\Action\ListActionFactory::class,
//                Authors\Model\AuthorRestCollection::class=>function($container) {
//                    $httpClient=$container->get(Authors\Model\AuthorHttpClient::class);
//                    return new Authors\Model\AuthorRestCollection($httpClient);
//                },
//                Authors\Model\AuthorHttpClient::class => function($container) {
//                    $config=$container->get('config');
//                    $client=new Client($config['httpclient']['base_uri'].
//                            $config['httpclient']['authors']['route']);
//                    $client->setHeaders($config['httpclient']['headers']);
//                    $client->setAuth(
//                            $config['httpclient']['basic_auth']['user'],
//                            $config['httpclient']['basic_auth']['password']
//                            );
//                    return $client;
//                },
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'author/view'    => [__DIR__ . '/../templates/view'],
                'author/error'  => [__DIR__ . '/../templates/error'],
                'author/layout' => [__DIR__ . '/../templates/layout'],
            ],
        ];
    }
}
